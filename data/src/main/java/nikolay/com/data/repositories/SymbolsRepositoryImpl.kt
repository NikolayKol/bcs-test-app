package nikolay.com.data.repositories

import io.reactivex.Single
import nikolay.com.data.entities.mappers.DTOToModelMapper
import nikolay.com.data.network.api.SymbolsApi
import nikolay.com.domain.models.Symbol
import nikolay.com.domain.repositories.SymbolsRepository

class SymbolsRepositoryImpl(
    private val toModelMapper: DTOToModelMapper,
    private val symbolsApi: SymbolsApi
) : SymbolsRepository {
    override fun loadSymbols(): Single<Pair<Long, List<Symbol>>> {
        return symbolsApi.symbols().map { Pair(it.serverTime, toModelMapper.transformSymbols(it.symbols)) }
    }
}