package nikolay.com.data.entities.dto

import com.google.gson.annotations.SerializedName

class SymbolDTO(
    @SerializedName("symbol") val symbol: String,
    @SerializedName("baseAsset") val baseAsset: String,
    @SerializedName("quoteAsset") val quoteAsset: String
)