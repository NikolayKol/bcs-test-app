package nikolay.com.data.entities.mappers

import nikolay.com.data.entities.dto.SymbolDTO
import nikolay.com.domain.models.Symbol
import javax.inject.Inject

class DTOToModelMapper
@Inject
constructor() {

    fun transformSymbols(dtoList: List<SymbolDTO>): List<Symbol> {
        val symbols = mutableListOf<Symbol>()
        dtoList.forEach { symbols.add(transformSymbol(it)) }
        return symbols
    }

    private fun transformSymbol(dto: SymbolDTO): Symbol {
        return Symbol(dto.symbol, dto.baseAsset, dto.quoteAsset)
    }
}