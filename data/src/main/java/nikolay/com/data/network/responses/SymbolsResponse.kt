package nikolay.com.data.network.responses

import com.google.gson.annotations.SerializedName
import nikolay.com.data.entities.dto.SymbolDTO

data class SymbolsResponse(
    @SerializedName("serverTime") val serverTime: Long,
    @SerializedName("symbols") val symbols: List<SymbolDTO>
)