package nikolay.com.data.network.api

import io.reactivex.Single
import nikolay.com.data.network.responses.SymbolsResponse
import retrofit2.http.GET

interface SymbolsApi {
    @GET("/api/v1/exchangeInfo")
    fun symbols(): Single<SymbolsResponse>
}