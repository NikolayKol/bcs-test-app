package nikolay.com.domain.livedata

data class DataModel<T>(
    val success: Boolean,
    val data: T?,
    val throwable: Throwable?
) {
    companion object {
        fun <T> success(data: T) = DataModel(true, data, null)

        fun <T> fail(throwable: Throwable): DataModel<T> = DataModel(false, null, throwable = throwable)
    }
}