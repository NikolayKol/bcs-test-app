package nikolay.com.domain.repositories

import io.reactivex.Single
import nikolay.com.domain.models.Symbol

interface SymbolsRepository {
    fun loadSymbols(): Single<Pair<Long, List<Symbol>>>
}