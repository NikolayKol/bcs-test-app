package nikolay.com.domain.models

data class Symbol(val symbol: String, val baseAsset: String, val quoteAsset: String)