package nikolay.com.domain.usecases

import io.reactivex.Flowable
import io.reactivex.Single
import nikolay.com.domain.livedata.DataModel
import nikolay.com.domain.models.Symbol
import nikolay.com.domain.repositories.SymbolsRepository
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class SymbolsUseCases
@Inject
constructor(private val symbolsRepository: SymbolsRepository) {

    fun loadSymbolsEveryTreeSeconds(): Flowable<DataModel<Pair<Long, List<Symbol>>>> {
        return Flowable
            .interval(3, TimeUnit.SECONDS)
            .flatMap {
                symbolsRepository
                    .loadSymbols()
                    .map { DataModel.success(it) }
                    .onErrorResumeNext { Single.just(DataModel.fail(it)) }
                    .toFlowable()
            }
    }

}