package com.nikolay.testapp.app.di.usecases.modules

import dagger.Module
import dagger.Provides
import nikolay.com.data.network.api.SymbolsApi
import retrofit2.Retrofit

@Module
class ApiModule {
    @Provides
    fun symbolsApi(retrofit: Retrofit): SymbolsApi {
        return retrofit.create(SymbolsApi::class.java)
    }
}