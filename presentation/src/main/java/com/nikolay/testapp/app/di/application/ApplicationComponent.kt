package com.nikolay.testapp.app.di.application

import android.content.Context
import dagger.Component
import retrofit2.Retrofit

@Component(modules = [ApplicationModule::class])
interface ApplicationComponent {

    fun context(): Context

    fun retrofit(): Retrofit

}