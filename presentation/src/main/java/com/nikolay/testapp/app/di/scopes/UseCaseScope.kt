package com.nikolay.testapp.app.di.scopes

import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class UseCaseScope