package com.nikolay.testapp.app.di.usecases

import com.nikolay.testapp.app.di.application.ApplicationComponent
import com.nikolay.testapp.app.di.scopes.UseCaseScope
import com.nikolay.testapp.app.di.usecases.modules.ApiModule
import com.nikolay.testapp.app.di.usecases.modules.RepositoriesModule
import dagger.Component
import nikolay.com.domain.usecases.SymbolsUseCases

@UseCaseScope
@Component(
    dependencies = [ApplicationComponent::class],
    modules = [ApiModule::class, RepositoriesModule::class]
)
interface UseCasesComponent {
    fun symbolsUseCases(): SymbolsUseCases
}