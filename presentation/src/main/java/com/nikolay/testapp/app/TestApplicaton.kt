package com.nikolay.testapp.app

import android.app.Application
import com.nikolay.testapp.app.di.application.ApplicationComponent
import com.nikolay.testapp.app.di.application.ApplicationModule
import com.nikolay.testapp.app.di.application.DaggerApplicationComponent
import com.nikolay.testapp.app.di.usecases.DaggerUseCasesComponent
import com.nikolay.testapp.app.di.usecases.UseCasesComponent

class TestApplicaton : Application() {

    private val applicationComponent: ApplicationComponent by lazy {
        DaggerApplicationComponent
            .builder()
            .applicationModule(ApplicationModule(this))
            .build()
    }

    val useCasesComponent: UseCasesComponent by lazy {
        DaggerUseCasesComponent
            .builder()
            .applicationComponent(applicationComponent)
            .build()
    }

}