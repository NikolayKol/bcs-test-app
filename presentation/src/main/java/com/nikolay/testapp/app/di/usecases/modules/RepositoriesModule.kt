package com.nikolay.testapp.app.di.usecases.modules

import dagger.Module
import dagger.Provides
import nikolay.com.data.entities.mappers.DTOToModelMapper
import nikolay.com.data.network.api.SymbolsApi
import nikolay.com.data.repositories.SymbolsRepositoryImpl
import nikolay.com.domain.repositories.SymbolsRepository

@Module
class RepositoriesModule {

    @Provides
    fun symbolsRepository(toModelMapper: DTOToModelMapper, symbolsApi: SymbolsApi): SymbolsRepository {
        return SymbolsRepositoryImpl(toModelMapper, symbolsApi)
    }

}