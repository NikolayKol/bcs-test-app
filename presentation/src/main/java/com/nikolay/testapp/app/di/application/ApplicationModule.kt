package com.nikolay.testapp.app.di.application

import android.content.Context
import com.nikolay.testapp.R
import dagger.Module
import dagger.Provides
import io.reactivex.schedulers.Schedulers
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

@Module
class ApplicationModule(private val context: Context) {

    @Provides
    fun context(): Context = context

    @Provides
    fun retrofit(): Retrofit {
        val url = context.getString(R.string.base_server_url)

        return Retrofit
            .Builder()
            .baseUrl(url)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
            .build()
    }
}