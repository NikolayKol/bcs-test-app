package com.nikolay.testapp.root

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveDataReactiveStreams
import com.nikolay.testapp.app.TestApplicaton

class RootViewModel(application: Application) : AndroidViewModel(application) {

    val symbolsLiveData = LiveDataReactiveStreams
        .fromPublisher(
            (application as TestApplicaton)
                .useCasesComponent
                .symbolsUseCases()
                .loadSymbolsEveryTreeSeconds()
        )

}