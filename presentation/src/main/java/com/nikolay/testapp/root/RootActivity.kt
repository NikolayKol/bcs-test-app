package com.nikolay.testapp.root

import android.content.res.Configuration
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import com.nikolay.testapp.R
import com.nikolay.testapp.epoxy.SymbolsController
import com.nikolay.testapp.epoxy.SymbolsListener
import kotlinx.android.synthetic.main.activity_root.*
import nikolay.com.domain.livedata.DataModel
import nikolay.com.domain.models.Symbol

class RootActivity : AppCompatActivity(), Observer<DataModel<Pair<Long, List<Symbol>>>>, SymbolsListener {

    private val controller = SymbolsController(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_root)

        initRecycler()

        controller.showLoading(true)

        model().symbolsLiveData.observe(this, this)
    }

    override fun onChanged(dataModel: DataModel<Pair<Long, List<Symbol>>>) {
        if (dataModel.success) {
            controller.showLoading(false)
            controller.showData(dataModel.data!!)
        }
    }

    override fun symbolClicked(symbol: Symbol) {
        Toast.makeText(this, "symbol ${symbol.symbol} clicked", Toast.LENGTH_SHORT).show()
    }

    private fun initRecycler() {
        val spanCount = when (resources.configuration.orientation) {
            Configuration.ORIENTATION_LANDSCAPE -> 4
            else -> 2
        }

        val layoutManager = GridLayoutManager(this, spanCount)
        controller.spanCount = spanCount
        layoutManager.spanSizeLookup = controller.spanSizeLookup

        epoxy_recycler.setController(controller)
        epoxy_recycler.layoutManager = layoutManager
    }

    private fun model() = ViewModelProviders
        .of(this)
        .get(RootViewModel::class.java)
}