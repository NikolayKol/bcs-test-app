package com.nikolay.testapp.epoxy

import android.content.Context
import android.widget.FrameLayout
import com.airbnb.epoxy.ModelView
import com.nikolay.testapp.R

@ModelView(autoLayout = ModelView.Size.MATCH_WIDTH_WRAP_HEIGHT)
class LoadingView(context: Context) : FrameLayout(context) {
    init {
        inflate(context, R.layout.layout_loading_view, this)
    }
}