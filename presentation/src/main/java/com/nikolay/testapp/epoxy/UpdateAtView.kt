package com.nikolay.testapp.epoxy

import android.content.Context
import android.widget.FrameLayout
import com.airbnb.epoxy.ModelProp
import com.airbnb.epoxy.ModelView
import com.nikolay.testapp.R
import kotlinx.android.synthetic.main.layout_update_at.view.*
import java.text.SimpleDateFormat
import java.util.*

private val DATE_FORMAT = SimpleDateFormat("d MMM yyyy HH:mm:ss", Locale.forLanguageTag("ru"))

@ModelView(autoLayout = ModelView.Size.MATCH_WIDTH_WRAP_HEIGHT)
class UpdateAtView(context: Context) : FrameLayout(context) {

    init {
        inflate(context, R.layout.layout_update_at, this)
    }

    @ModelProp
    fun serverTime(serverTime: Long) {
        val date = Date(serverTime)
        update_at.text = context.getString(R.string.updated_at_placeholder, DATE_FORMAT.format(date))
    }
}