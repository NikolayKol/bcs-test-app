package com.nikolay.testapp.epoxy

import com.airbnb.epoxy.EpoxyController
import nikolay.com.domain.models.Symbol

private const val LOADING_VIEW_ID = "loading_view"
private const val UPDATE_AT_VIEW_ID = "update_at_view"

interface SymbolsListener {
    fun symbolClicked(symbol: Symbol)
}

class SymbolsController(private val listener: SymbolsListener) : EpoxyController() {

    private var loading = false

    private var data = Pair<Long, List<Symbol>>(0, emptyList())

    override fun buildModels() {
        LoadingViewModel_()
            .id(LOADING_VIEW_ID)
            .addIf(loading, this)

        UpdateAtViewModel_()
            .id(UPDATE_AT_VIEW_ID)
            .serverTime(data.first)
            .addIf(data.second.isNotEmpty(), this)

        data.second.forEach {
            SymbolViewModel_()
                .id(it.symbol)
                .symbol(it)
                .listener { listener.symbolClicked(it) }
                .spanSizeOverride { totalSpanCount, _, _ -> totalSpanCount / spanCount }
                .addTo(this)
        }

    }

    fun showLoading(show: Boolean) {
        loading = show
        requestModelBuild()
    }

    fun showData(data: Pair<Long, List<Symbol>>) {
        this.data = data
        requestModelBuild()
    }
}