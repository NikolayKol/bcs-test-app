package com.nikolay.testapp.epoxy

import android.content.Context
import android.widget.FrameLayout
import com.airbnb.epoxy.ModelProp
import com.airbnb.epoxy.ModelView
import com.nikolay.testapp.R
import kotlinx.android.synthetic.main.layout_symbol.view.*
import nikolay.com.domain.models.Symbol

@ModelView(autoLayout = ModelView.Size.MATCH_WIDTH_WRAP_HEIGHT)
class SymbolView(context: Context) : FrameLayout(context) {
    init {
        inflate(context, R.layout.layout_symbol, this)
    }

    @ModelProp
    fun symbol(symbol: Symbol) {
        text.text = context.getString(R.string.symbols_placeholder, symbol.baseAsset, symbol.quoteAsset)
    }

    @ModelProp(options = [ModelProp.Option.DoNotHash])
    fun listener(listener: () -> Unit) {
        setOnClickListener { listener.invoke() }
    }
}